<?php

require __DIR__ . "/../vendor/autoload.php";
require __DIR__ . "/../src/HttpConnector.php";

use Lcobucci\JWT\Token;
use Lcobucci\JWT\Builder;
use s2AuthComponent\s2AuthComponent;
use PHPUnit\Framework\TestCase;

class s2AuthComponentTest extends TestCase {

    /**
     * Test authenticate Method
     */
    public function testValidateOptionsAndErrorException(){

        $expectedCredentials = 'ANY_CREDENTIALS';
        $expectedErrorMessage = 'ANY_ERROR_MESSAGE';

        $expectedCurlOptions = array(
                10002 => 'https://myauthurl/api/authenticate',
                19913 => true,
                10023 => array (
                    0 => "Authorization: Basic $expectedCredentials",
                    1 => 'Content-type: text/plain',
                    2 => 'Content-length: 0'
                ),
                64 => false,
                13 => 60,
                47 => true
        );

        $http = $this->getMockBuilder('HttpConnector')
            ->setMethods(array('execute','setOptions','getInfo','getError','getErrorMessage','close'))
            ->getMock();

        $http->expects($this->once())->method('setOptions')->with($expectedCurlOptions);
        $http->expects($this->once())->method('getError')->will($this->returnValue('bar'));
        $http->expects($this->once())->method('getErrorMessage')->will($this->returnValue($expectedErrorMessage));

        $this->expectException('Exception');
        $this->expectExceptionMessage($expectedErrorMessage);

        $authComponent = new s2AuthComponent();
        $authComponent->setupHttpConnector($http);
        $authComponent->authenticate($expectedCredentials);
    }

    public function testHttpStatusCodeOtherThan200(){

        $returnInfo = array(
            'http_code' => 500
        );

        $expectedResponse = 'any_response';

        $http = $this->getMockBuilder('HttpConnector')
            ->setMethods(array('execute','setOptions','getInfo','getError','getErrorMessage','close'))
            ->getMock();

        $http->expects($this->once())->method('getInfo')->will($this->returnValue($returnInfo));
        $http->expects($this->once())->method('execute')->will($this->returnValue($expectedResponse));

        $this->expectException('Exception');
        $this->expectExceptionMessage($expectedResponse);

        $authComponent = new s2AuthComponent();
        $authComponent->setupHttpConnector($http);
        $authComponent->authenticate('any_base_64_value');
    }

    public function testReceiveCorrectResponse(){

        $expectedResponse = 'ANY_TOKEN';

        $http = $this->getMockBuilder('HttpConnector')
            ->setMethods(array('execute','setOptions','getInfo','getError','getErrorMessage','close'))
            ->getMock();

        $http->expects($this->once())->method('getInfo')->will($this->returnValue(array('http_code' => 200)));
        $http->expects($this->once())->method('execute')->will($this->returnValue($expectedResponse));

        $authComponent = new s2AuthComponent();
        $authComponent->setupHttpConnector($http);
        $response = $authComponent->authenticate('any_base_64_value');
        $this->assertEquals($response, $expectedResponse);
    }

    /**
     * Test validateS2AuthToken Method
     */

    public function testReturnFalseIfNecessaryRefreshToken(){

        $token = (new Builder())->setIssuer('http://example.com')
                        ->setAudience('http://example.org')
                        ->setId('4f1g23a12aa', true)
                        ->setIssuedAt(time())
                        ->setNotBefore(time() + 60)
                        ->setExpiration(time() - 3600)
                        ->set('uid', 1)
                        ->getToken();

        $authComponent = new s2AuthComponent();
        $this->assertFalse($authComponent->validatesS2AuthToken($token));
    }

    public function testReturnTrueIfNotNecessaryRefreshToken(){

        $token = (new Builder())->setIssuer('http://example.com')
                        ->setAudience('http://example.org')
                        ->setId('4f1g23a12aa', true)
                        ->setIssuedAt(time())
                        ->setNotBefore(time() + 60)
                        ->setExpiration(time() + 3600)
                        ->set('uid', 1)
                        ->getToken();

        $authComponent = new s2AuthComponent();
        $this->assertTrue($authComponent->validatesS2AuthToken($token));
    }

    public function testReturnCorrectExtractedField(){

        $jwtData = (new Builder())->setIssuer('http://example.com')
                        ->setAudience('http://example.org')
                        ->setId('4f1g23a12aa', true)
                        ->setIssuedAt(time())
                        ->setNotBefore(time() + 60)
                        ->setExpiration(time() + 3600)
                        ->set('field1', 'value1')
                        ->set('field2', 'value2')
                        ->getToken();

        $jwt = (string) $jwtData;

        $authComponent = new s2AuthComponent();

        $this->assertEquals($authComponent->extractField($jwt, 'field1'), 'value1');
        $this->assertEquals($authComponent->extractField($jwt, 'field2'), 'value2');
    }
}
